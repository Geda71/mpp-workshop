package ui;

import javafx.application.Application;
import javafx.stage.Stage;
import library.Controller;
import library.UIController;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        UIController uiController = UIController.getInstance();
        uiController.setStage(primaryStage);

        Controller controller = new Controller(uiController);

    }


    public static void main(String[] args) {
        launch(args);
    }
}
