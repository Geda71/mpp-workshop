package dataAccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 985366 on 9/13/2016.
 */
public class DataAccessImpl<K, V> implements DataAccess<K, V> {

    private Map<K, V> dataMap = new HashMap<K, V>();

    public boolean add(K key, V value){
        return (dataMap.put(key, value) == value);
    }
    public boolean update(K key, V value){
        return (dataMap.put(key, value) == value);
    }
    public V get(K key){
        return dataMap.get(key);
    }
    public List<V> getAll() {return  new ArrayList<V>(dataMap.values()); }
    public boolean delete(K key){
        return dataMap.remove(key) == dataMap.get(key);
    }
}
