package dataAccess;

import java.util.List;

/**
 * Created by 985366 on 9/13/2016.
 */
public interface DataAccess<K, V> {
    public boolean add(K key, V value);
    public boolean update(K key, V value);
    public V get(K key);
    public List<V> getAll();
    public boolean delete(K key);
}
