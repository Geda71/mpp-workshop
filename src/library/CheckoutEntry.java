package library;

import java.time.LocalDate;

/**
 * Created by geda on 9/13/16.
 */
public class CheckoutEntry {
    private LocalDate checkoutDate;
    private LocalDate dueDate;
    private LocalDate dateReturned;

    public void setDateReturned(LocalDate dateReturned) {
        this.dateReturned = dateReturned;
    }

    public Copy copy;

    public CheckoutEntry(LocalDate checkoutDate, LocalDate dueDate, Copy copy) {
        this.checkoutDate = checkoutDate;
        this.dueDate = dueDate;
        this.dateReturned = dateReturned;
        this.copy = copy;
    }

    public LocalDate getCheckoutDate() {
        return checkoutDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public LocalDate getDateReturned() {
        return dateReturned;
    }
}
