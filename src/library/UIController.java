package library;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ui.SCENES;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by geda on 9/13/16.
 */
public class UIController {
    private static UIController instance = null;
    private static Stage stage;
    private static Controller controller;
    private static final int SCENE_WIDTH = 600;
    private static final int SCENE_HEIGHT = 480;

    @FXML
    private TextField txtUsername;
    @FXML
    private TextField txtPassword;
    //Add book controls
    @FXML
    private ComboBox selectAuthor;
    @FXML
    private TextField txtBookTitle;
    @FXML
    private TextField txtIsbn;
    //Checkout book controls
    @FXML
    private ComboBox selectchkBookISBN;
    @FXML
    private ComboBox selectchkMember;
    //Search book controls
    @FXML
    private TextField txtSearchBook;
    @FXML
    private TableView tlbViewBookResult;

    private static Parent root;

    public static UIController getInstance() {
        if (instance == null) {
           instance = new UIController();
        }
        return instance;
    }

    public UIController() {
    }

    public Controller getController() {
        return this.controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void loginForm() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../ui/loginForm.fxml"));
        this.stage.setTitle("Library System");
        this.stage.setScene(new Scene(root, SCENE_WIDTH, SCENE_HEIGHT));
        this.stage.show();
    }

    @FXML
    public void submitLogin(ActionEvent event) {
        this.controller.Login(txtUsername.getText(), txtPassword.getText());
    }

    public void setLoginError(String errorMsg) {
        Text txtErrorMsg = (Text) this.getStage().getScene().lookup("#txtErrorMsg");
        txtErrorMsg.setText(errorMsg);
        txtErrorMsg.setFill(Color.web("#ff0000"));
    }

    public Parent getLoginNode() throws Exception{
        if (this.root == null)
        this.root = FXMLLoader.load(getClass().getResource("../ui/loginForm.fxml"));
        return root;
    }

    public Parent getParentNode(String xmlUrl) {
        try {
            return FXMLLoader.load(getClass().getResource(xmlUrl));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setSceneForm(SCENES enumScene) {
        try {
            switch( SCENES.valueOf( enumScene.name()) ) {
                case LIB_MAIN_FORM:
                    this.getStage().setScene(new Scene(getParentNode("../ui/LibMainForm.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
                case ADMIN_MAIN_FORM:
                    this.getStage().setScene(new Scene(getParentNode("../ui/MainForm.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
                case LIB_BOTH_ROLES:
                    this.getStage().setScene(new Scene(getParentNode("../ui/BothRoles.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
                case ADD_BOOK:
                    this.getStage().setScene(new Scene(getParentNode("../ui/addBook.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    initAddBook();
                    break;
                case ADD_MEMBER:
                    this.getStage().setScene(new Scene(getParentNode("../ui/addNewMember.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
                case ADD_CHECKOUT:
                    this.getStage().setScene(new Scene(getParentNode("../ui/checkoutRecord.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    initCheckOutForm();
                    break;
                case SEARCH_BOOK:
                    this.getStage().setScene(new Scene(getParentNode("../ui/searchBook.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
                case ADD_BOOK_COPY:
                    this.getStage().setScene(new Scene(getParentNode("../ui/addCopy.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    initBookCopy();
                    break;
                case LOGIN:
                    this.getStage().setScene(new Scene(getParentNode("../ui/loginForm.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
                case CHECKOUT_RECORD_DETAILS:
                    this.getStage().setScene(new Scene(getParentNode("../ui/checkoutRecordDetail.fxml"),
                            SCENE_WIDTH, SCENE_HEIGHT));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCheckoutRecordDetails(CheckoutEntry chkEntry){
        setSceneForm(SCENES.CHECKOUT_RECORD_DETAILS);
        Text isbnText = (Text) this.getStage().getScene().lookup("#isbn");
        isbnText.setText(chkEntry.copy.getBook().getIsbn());
        Text titleText = (Text) this.getStage().getScene().lookup("#title");
        titleText.setText(chkEntry.copy.getBook().getTitle());
        Text copy_idText = (Text) this.getStage().getScene().lookup("#copy_id");
        copy_idText.setText(chkEntry.copy.getId());
        Text nameText = (Text) this.getStage().getScene().lookup("#name");
        nameText.setText(chkEntry.copy.getMember().toString());
        Text chkOutText = (Text) this.getStage().getScene().lookup("#checked_out");
        chkOutText.setText(chkEntry.getCheckoutDate().toString());
        Text dueDateText = (Text) this.getStage().getScene().lookup("#dueDate");
        dueDateText.setText(chkEntry.getDueDate().toString());
    }

    public void initAddBook() {
        ComboBox selectAuthor = (ComboBox) this.getStage().getScene().lookup("#selectAuthor");
        ObservableList<Author> siteList =
                FXCollections.observableArrayList(controller.getAuthorList());
        selectAuthor.setItems(siteList);
        System.out.println(controller.getAuthorList());
    }

    @FXML
    public void saveBook() {
        //add book with isbn, title, and authors
        boolean success = controller.addBook(txtIsbn.getText(), txtBookTitle.getText(),
                ((Author)selectAuthor.getSelectionModel().getSelectedItem())
                        .getPerson().getId());
        if (success) saveSuccessForm();

    }

    @FXML
    public void checkOutBook() {
        Member member = (Member) selectchkMember.getSelectionModel().getSelectedItem();
        Book book = (Book) selectchkBookISBN.getSelectionModel().getSelectedItem();
        System.out.println(member.getId() + " " + book.getIsbn());
        controller.checkoutBook(member.getId(), book.getIsbn());
    }

    private void initCheckOutForm() {
        ComboBox selectMembers = (ComboBox) this.getStage().getScene().lookup("#selectchkMember");
        ObservableList<Member> memberList =
                FXCollections.observableArrayList(controller.getMemberList());
        selectMembers.setItems(memberList);

        ComboBox selectIsbn = (ComboBox) this.getStage().getScene().lookup("#selectchkBookISBN");
        ObservableList<Book> bookList =
                FXCollections.observableArrayList(controller.getBookList());
        selectIsbn.setItems(bookList);
    }

    public void checkoutError(String errorMsg) {
        Text txtCheckOutError = (Text) this.getStage().getScene().lookup("#txtCheckoutError");
        txtCheckOutError.setText(errorMsg);
        txtCheckOutError.setFill(Color.web("#ff0000"));
    }

    @FXML
    public void searchBook() {
        String searchStr = txtSearchBook.getText();
        List<Book> books = controller.searchBooksByISBN(searchStr);


//        Book b = new Book("123", "ABC");
//        Person p = new Person("Maryam", "Naraghi");
//        Author a = new Author(p, "shortBBIIOO");
//        b.addAuthor(a);
//        books.add(b);

        ObservableList<Book> observableBooks = FXCollections.observableList(books);

        tlbViewBookResult.setItems(observableBooks);
    }

    @FXML
    public void addMemberForm() {
        setSceneForm(SCENES.ADD_MEMBER);
        setTitleStyle("#txtAddMembertTitle");
        TextField txtZip = (TextField) this.getStage().getScene().lookup("#txtZip");
        txtZip.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    txtZip.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    @FXML
    public void searchMemberForm() {
        setSceneForm(SCENES.SEARCH_BOOK);
    }

    @FXML
    public void addBookForm() {
        setSceneForm(SCENES.ADD_BOOK);
        setTitleStyle("#txtAddBooktTitle");
    }

    @FXML
    public void checkoutBookForm() {
        setSceneForm(SCENES.ADD_CHECKOUT);
        setTitleStyle("#txtCheckoutTitle");
    }

    @FXML
    public void searchBookForm() {
        setSceneForm(SCENES.SEARCH_BOOK);
    }

    @FXML
    public void addBookCopyForm() {
        setSceneForm(SCENES.ADD_BOOK_COPY);
        setTitleStyle("#txtAddCopyTitle");
    }
    @FXML
    public void backHome() {
        if (controller.getCurrentUser().getLibrarian() != null && controller.getCurrentUser().getAdministrator() != null) {
            setSceneForm(SCENES.LIB_BOTH_ROLES);
        } else if (controller.getCurrentUser().getAdministrator() != null) {
            setSceneForm(SCENES.ADMIN_MAIN_FORM);
        }
        else if (controller.getCurrentUser().getLibrarian() != null) {
            setSceneForm(SCENES.LIB_MAIN_FORM);
        } else {
            setLoginError("Not Admin or Librarian");
        }
    }
    //add Member controls
    @FXML TextField txtMemUsername;
    @FXML TextField txtFirstName;
    @FXML TextField txtLastName;
    @FXML TextField txtPhoneNo;
    @FXML TextField txtStreet;
    @FXML TextField txtCity;
    @FXML TextField txtState;
    @FXML TextField txtZip;
    @FXML
    public void addNewMember() {
        //String id, String fn, String ln, String phone, String street, String city, String state, int zip
        controller.addMember(txtMemUsername.getText(), txtFirstName.getText(), txtLastName.getText(),
                txtPhoneNo.getText(), txtStreet.getText(), txtCity.getText(), txtState.getText(), Integer.parseInt(txtZip.getText()));
    }

    public void memberError(String errorMsg) {
        Text txtMemberError = (Text) this.getStage().getScene().lookup("#txtMemberError");
        txtMemberError.setText(errorMsg);
        txtMemberError.setFill(Color.web("#ff0000"));
    }

    //Add Copy Controls
    @FXML
    ComboBox selectCopyIsbn;
    @FXML
    TextField txtCopyID;
    @FXML
    ComboBox selectDayAllow;
    @FXML
    private void addBookCopy() {
        boolean success = controller.addCopy(((Book) selectCopyIsbn.getSelectionModel().getSelectedItem())
                .getIsbn(), txtCopyID.getText(),
                Integer.parseInt(selectDayAllow.getSelectionModel().getSelectedItem().toString()) );
        if (success) saveSuccessForm();
    }

    private void initBookCopy(){
        ComboBox selectCopyIsbn = (ComboBox) this.getStage().getScene().lookup("#selectCopyIsbn");
        ObservableList<Book> bookList = FXCollections.observableArrayList(controller.getBookList());
        selectCopyIsbn.setItems(bookList);
    }

    /**
     * Display list of Entity eg: Member, Book
     * @param objectList entityList to pass
     * @param headerList List of column Headers
     * @return
     */
    private Parent showListView(List<?> objectList, HashMap<String, String> headerList) {
        VBox vbox = new VBox();
        TableView<?> tableView = new TableView<>();
        headerList.forEach((k, v) -> {
            System.out.println("Item : " + k + " Count : " + v);
//            TableColumn<?, String> column = new TableColumn<?, String>();
//            column.setCellValueFactory(new PropertyValueFactory<?, String>(v));
           // tableView.getColumns().add(column);
        });

       for (Object obj : objectList) {
            headerList.forEach((k, v)->{

            //FXCollections.checkedObservableList();
                FXCollections.observableArrayList(
                           objectList
                );
            });

       }
        vbox.getChildren().clear();
        vbox.getChildren().setAll(tableView);
        return (Parent) vbox;
    }

    public void displayTableList (List<Object> objectList, HashMap<String, String> headers) {
        Parent root = showListView(objectList, headers);
        Scene listScene = new Scene(root, SCENE_WIDTH, SCENE_HEIGHT);
        this.getStage().setScene(listScene);
    }

    public void setTitleStyle(String txtId) {
        Text txtTitle = (Text) this.getStage().getScene().lookup(txtId);
        txtTitle.setFont(Font.font ("FontWeight.BOLD", 17));
    }

    public void saveSuccessForm () {
       // VBox successVbox = new VBox();
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        Text textSuccess = new Text("Save Successfully");
        textSuccess.setFont(Font.font ("FontWeight.BOLD", 35));
        Button buttonOk = new Button();
        buttonOk.setText("OK");
        buttonOk.setMaxWidth(Double.MAX_VALUE);
        buttonOk.setFont(Font.font ("FontWeight.BOLD", 30));
        gridPane.add(textSuccess, 0 , 0);
        gridPane.add(buttonOk, 0, 1);
        buttonOk.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backHome();
            }
        });

        this.getStage().setScene(new Scene(gridPane, SCENE_WIDTH, SCENE_HEIGHT));
    }

    @FXML
    private void logOutForm () {
       this.getController().logOut();
       setSceneForm(SCENES.LOGIN);
    }
}
