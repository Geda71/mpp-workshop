package library;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by geda on 9/13/16.
 */
public class Book {
    private String isbn;
    private String title;
    private List<Author> authors;
    private List<Copy> copies;

    private int daysAllowed;

    {
        authors = new ArrayList<Author>();
        copies = new ArrayList<Copy>();
    }
    public Book(String isbn, String title) {
        this.isbn = isbn;
        this.title = title;
    }

    public void addAuthor(Author author) {
        author.addBook(this);
        this.authors.add(author);
    }

    public int getDaysAllowed() {
        return daysAllowed;
    }

    public void setDaysAllowed(int daysAllowed) {
        this.daysAllowed = daysAllowed;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setCopies(List<Copy> copies) {
        this.copies = copies;
    }

    public void addCopy(Copy c) {
        this.copies.add(c);
    }

    public String getIsbn() {
        return isbn;
    }
    public StringProperty isbnProperty() {
        StringProperty isbnProperty = new SimpleStringProperty();
        isbnProperty.set(isbn);
        return isbnProperty;
    }


    public StringProperty authorProperty() {
        StringProperty authorProperty = new SimpleStringProperty();
        String CSVAuthors = "";
        for(Author author: getAuthors())
        {
            CSVAuthors += author.getPerson().getFirstName() + " " + author.getPerson().getLastName() + ", ";
        }
        authorProperty.set(CSVAuthors);
        return authorProperty;
    }


    public String getTitle() {
        return title;
    }

    public StringProperty titleProperty() {
        StringProperty titleProperty = new SimpleStringProperty();
        titleProperty.set(title);
        return titleProperty;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public List<Copy> getCopies() {
        return copies;
    }

    public Copy getAvailableCopy() {
        for (Copy c : copies) {
            if (!c.isBorrowed())
                return c;
        }
        return null;
    }


    public boolean isAvailable()
    {
        return getAvailableCopy() != null;
    }

    public List<String> getAuthorNames() {
        List<String> authors = new ArrayList<String>();
        for (Author a : this.getAuthors()) {
            authors.add(a.getPerson().getLastName());
        }
        return authors;
    }

    @Override
    public String toString() {
        return this.getTitle()+" by " + String.join(",", getAuthorNames());
    }
}
