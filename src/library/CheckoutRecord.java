package library;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by geda on 9/13/16.
 */
public class CheckoutRecord {
    private List<CheckoutEntry> entries;
    private Member member;

    {
        this.entries = new ArrayList<CheckoutEntry>();
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public boolean addEntry (CheckoutEntry e) {
        return this.entries.add(e);
    }

    public List<CheckoutEntry> getEntries() {
        return this.entries;
    }
}
