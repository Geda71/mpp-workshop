package library;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by geda on 9/13/16.
 */
public class User {
    private String userName;
    private String password;

    public Person person;

    private List<Role> roles;

    {
        this.roles = new ArrayList<Role>();
    }

    public void addRole(Role r) {
        this.roles.add(r);
    }

    public Role getAdministrator() {
        for (Role r : this.roles) {
            if (r instanceof Administrator)
                return r;
        }
        return null;
    }

    public Role getLibrarian() {
        for (Role r : this.roles) {
            if (r instanceof Librarian)
                return r;
        }
        return null;
    }

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
//
//    public void setId(String id) {
//        this.id = id;
//    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return userName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword()
    {
        return password;
    }

    public boolean isAdmin()
    {
        return getAdministrator() != null;
    }

    public boolean isLibrarian()
    {
        return getLibrarian() != null;
    }

}
