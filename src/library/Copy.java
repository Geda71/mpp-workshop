package library;

/**
 * Created by geda on 9/13/16.
 */
public class Copy {
    private String id;
    private boolean borrowed;
    private Member member; //necessary??
    private int daysAllowed;

    public void setDaysAllowed(int daysAllowed) {
        this.daysAllowed = daysAllowed;
    }

    public int getDaysAllowed() {

        return daysAllowed;
    }

    public Book book;

    {
        this.borrowed = false;
    }

    public Copy(Book book, String id) {
        this.book = book;
        this.id = id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBorrowed(boolean borrowed) {
        this.borrowed = borrowed;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getId() {
        return id;
    }

    public boolean isBorrowed() {
        return borrowed;
    }

    public Member getMember() {
        return member;
    }

    public Book getBook(){ return this.book; }
}
