package library;

/**
 * Created by geda on 9/13/16.
 */
public class Member {
    private CheckoutRecord checkoutRecord;
    public Person person;

    {
        this.checkoutRecord = new CheckoutRecord();
    }

    public Member(String id, Person person) {
        this.person = person;
        this.person.setId(id);
    }

    public String getId() {
        return this.person.getId();
    }

    public CheckoutRecord getCheckoutRecord() {
        return checkoutRecord;
    }

    public void setCheckoutRecord(CheckoutRecord checkoutRecord) {
        this.checkoutRecord = checkoutRecord;
    }

    public Person getPerson() {
        return person;
    }

    @Override
    public String toString() {
        return this.getPerson().getFirstName() + " " + this.getPerson().getLastName();
    }
}
