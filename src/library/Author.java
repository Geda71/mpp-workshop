package library;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by geda on 9/13/16.
 */
public class Author {
    private String shortBio;
    private Person person;

    private List<Book> books;

    {
        this.books = new ArrayList<Book>();
    }

    public void addBook(Book book) {
        this.books.add(book);
    }

    public Author() {
    }

    public Author(Person person, String shortBio) {
        this.person = person;
        this.shortBio = shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public String getShortBio() {
        return shortBio;
    }

    public Person getPerson() {
        return person;
    }

    @Override
    public String toString() {
        return this.getPerson().getFirstName() + " " + this.getPerson().getLastName();
    }
}
