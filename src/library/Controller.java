package library;

import controller.*;
import ui.SCENES;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    private UIController uiController;
    private User currentUser;
    private BookController bookDataAccess;
    private MemberController memberDataAccess;
    private UserController userDataAccess;
    private PersonController personDataAccess;
    private AutherController authorDataAccess;

    {
        this.userDataAccess = UserController.getInstance();
        this.bookDataAccess = BookController.getInstance();
        this.memberDataAccess = MemberController.getInstance();
        this.personDataAccess = PersonController.getInstance();
        this.authorDataAccess = AutherController.getInstance();

    }

    public Controller(UIController uiController) throws IOException {
        this.uiController = uiController;
        this.uiController.setController(this);

        //FOR TESTING PURPOSES
        Person p1 = new Person("Admin", "Administrator");
        User u1 = new User("admin", "pass");
        p1.user = u1;
        u1.person = p1;
        Role admin = new Administrator();
        u1.addRole(admin);
        this.personDataAccess.addPerson(p1);
        this.userDataAccess.addUser(u1);


        Person p2 = new Person("Lib", "Librarian");
        User u2 = new User("lib", "pass");
        p2.user = u2;
        u2.person = p2;
        Role lib = new Librarian();
        u2.addRole(lib);
        this.personDataAccess.addPerson(p2);
        this.userDataAccess.addUser(u2);


        Person p3 = new Person("Librarian", "Administrator");
        User u3 = new User("both", "pass");
        p3.user = u3;
        u3.person = p3;
        u3.addRole(admin);
        u3.addRole(lib);
        this.personDataAccess.addPerson(p3);
        this.userDataAccess.addUser(u3);

        addAuthor("Marwan","G","Talanted author","");
        addAuthor("Bishal","P","Talanted author","");
        addAuthor("Khemroat","L","Talanted author","");

        addBook("0000000001","First Book", "MarwanG");
        addBook("0000000002","Second Book", "BishalP");
        addBook("0000000003","Third Book", "KhemroatL");

        addMember("member1","Member","First","","1000 N 1st","Fairfield","Iowa",53214);
        addMember("member2","Member","Second","","1000 N 2st","Fairfield","Iowa",53214);
        addMember("member3","Member","Third","","1000 N 3st","Fairfield","Iowa",53214);
        addMember("member4","Member","Forth","","1000 N 4st","Fairfield","Iowa",53214);
        //END TESTING BLOCK


        uiController.loginForm();
    }


    public User getCurrentUser() {
        return this.currentUser;
    }

    public boolean Login(String id, String pass) {

        User user = userDataAccess.getUser(id);
        if (user == null || !user.getPassword().equals(pass)) {
            this.uiController.setLoginError("ID and Password missmatch!");
            return false;
        } else {
            this.currentUser = user;
            this.displayMainUI();
            return true;
        }
    }

    private void displayMainUI() {
        if (this.currentUser.getLibrarian() != null && this.currentUser.getAdministrator() != null) {
            this.uiController.setSceneForm(SCENES.LIB_BOTH_ROLES);
        } else if (this.currentUser.getAdministrator() != null) {
            this.uiController.setSceneForm(SCENES.ADMIN_MAIN_FORM);
        }
        else if (this.currentUser.getLibrarian() != null) {
            this.uiController.setSceneForm(SCENES.LIB_MAIN_FORM);
        } else {
            this.uiController.setLoginError("Not Admin or Librarian");
        }


    }

    public boolean memberExists(String id) {
        return (this.memberDataAccess.getMember(id) == null)? false : true;
    }

    public void addMember(String id, String fn, String ln, String phone, String street, String city, String state, int zip){
        if (memberExists(id)){
            this.uiController.memberError("Member already exists");
            return;
        }
        Address address = new Address(street, city, state, zip);
        Person person = new Person(fn,ln);
        person.setPhone(phone);
        person.setAddress(address);
        Member member = new Member(id, person);
        person.member = member;
        this.personDataAccess.addPerson(person);
        this.memberDataAccess.addMember(member);
        this.uiController.saveSuccessForm();

    }

    public List<Member> getMemberList(){
        return this.memberDataAccess.getAll();
    }

    public List<Author> getAuthorList() {
        return this.authorDataAccess.getAll();
    }

    public List<Book> getBookList() {
        return this.bookDataAccess.getAll();
    }

    public List<Book> searchBooksByISBN(String isbn)
    {
        List<Book> books = new ArrayList();
//        System.out.println(books);
        for(Book book: getBookList())
        {
            if(book.getIsbn().equals(isbn))
            {
                books.add(book);
            }
        }

        return books;
    }

    public boolean checkoutBook(String memberID, String ISBN) {

        Member member = this.memberDataAccess.getMember(memberID);
        if (member == null) {
            this.uiController.checkoutError("Member not found");
            return false;
        }

        Book book = this.bookDataAccess.getBook(ISBN);
        if (book == null) {
            this.uiController.checkoutError("Book not found");
            return false;
        } else {
            Copy copy = book.getAvailableCopy();
            if (copy == null) {
                this.uiController.checkoutError("No available copies");
                return false;
            } else {
                CheckoutEntry checkoutEntry = new CheckoutEntry(LocalDate.now(), LocalDate.now().plusDays(copy.getDaysAllowed()), copy);
                member.getCheckoutRecord().addEntry(checkoutEntry);
                this.memberDataAccess.updateMember(member);
                copy.setBorrowed(true);
                copy.setMember(member);
                this.bookDataAccess.updateBook(book);

                this.uiController.showCheckoutRecordDetails(checkoutEntry);
                return true;
            }
        }
    }

    public boolean addCopy(String ISBN, String id, int daysAllowed) {
        Book book = this.bookDataAccess.getBook(ISBN);
        Copy copy = new Copy(book, id);
        copy.setDaysAllowed(daysAllowed);
        book.addCopy(copy);

        return this.bookDataAccess.updateBook(book);
    }

    public boolean addAuthor(String firstName, String lastName, String shortBio, String phone) {
        Person person = new Person(firstName, lastName);
        person.setPhone(phone);
        person.setId(firstName+lastName);
        Author author = new Author();
        author.setShortBio(shortBio);
        author.setPerson(person);
        this.personDataAccess.addPerson(person);
        this.authorDataAccess.addAuthor(author);
        return true;
    }

    public boolean addBook(String isbn, String title, String authors)
    {
        Book book = new Book(isbn, title);
//        for (int ai : authors) {
            Author author = this.authorDataAccess.getAuthor(authors);
            book.addAuthor(author);
//        }
        this.bookDataAccess.addBook(book);
        return true;
    }

    public void logOut() {
        this.currentUser = null;
    }
}