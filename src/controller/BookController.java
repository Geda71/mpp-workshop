package controller;

import dataAccess.DataAccess;
import dataAccess.DataAccessImpl;
import library.Book;

import java.util.List;

/**
 * Created by 985366 on 9/13/2016.
 */
public class BookController {
    private static BookController instance = new BookController();
    private DataAccess<String, Book> books = new DataAccessImpl<String, Book>();

    private BookController(){};

    public static BookController getInstance()
    {
        return instance;
    }

    public boolean addBook(Book book)
    {
        return books.add(book.getIsbn(), book);
    }

    public Book getBook(String id)
    {
        return books.get(id);
    }

    public List<Book> getAll()
    {
        return books.getAll();
    }


    public boolean updateBook(Book book) {
        return books.update(book.getIsbn(), book);
    }

    public DataAccess<String, Book> getBooks()
    {
        return books;
    }

}
