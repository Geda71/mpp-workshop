package controller;

import dataAccess.DataAccess;
import dataAccess.DataAccessImpl;
import library.User;

/**
 * Created by 985366 on 9/13/2016.
 */
public class UserController {
    private static UserController instance = new UserController();
    private User currentUser;
    private DataAccess<String, User> users = new DataAccessImpl<String, User>();

    private UserController(){};

    public static UserController getInstance()
    {
        return instance;
    }

    public boolean addUser(User user) {
        return this.users.add(user.getId(), user);
    }

    public boolean Login(String id, String password)
    {
        if(currentUser != null)
        {
            //return show message or logout
            return false;
        }

        User newUser = getUser(id);
        if(newUser == null)
        {
            //return user not found message;
            return false;
        }

        if( ! newUser.getPassword().equals(password))
        {
            //return password unmatched message;
            return false;
        }

        currentUser = newUser;

        return displayMainUI();
    }

    private boolean displayMainUI()
    {
        if(currentUser.isAdmin())
        {
            //show admin ui and return true
        }
        else if(currentUser.isLibrarian())
        {
            //show librarian ui and return true
        }

        return false;
    }

    public User getUser(String id){
        return users.get(id);
    }
}
