package controller;

import dataAccess.DataAccess;
import dataAccess.DataAccessImpl;
import library.Book;
import library.CheckoutEntry;
import library.Copy;
import library.Member;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Created by 985366 on 9/13/2016.
 */
public class MemberController {
    private static MemberController instance = new MemberController();
    private DataAccess<String, Member> members = new DataAccessImpl<String,  Member>();

    private MemberController(){};

    public static MemberController getInstance()
    {
        return instance;
    }

    public boolean addMember(Member member)
    {
        return members.add(member.getId(), member);
    }

    public Member getMember(String id)
    {
        return members.get(id);
    }

    public List<Member> getAll()
    {
        return members.getAll();
    }

    public boolean updateMember(Member member) {
        return members.update(member.getId(), member);
    }

    public DataAccess<String, Member> getMembers()
    {
        return members;
    }

    public boolean checkout(String member_id, String isbn_number)
    {
        Book book = BookController.getInstance().getBook(isbn_number);
        Member member = getMember(member_id);

        if(! book.isAvailable())
        {
            //display message for book not available
            return false;
        }

        Copy copyOfBook = book.getAvailableCopy();
        CheckoutEntry checkoutEntry = new CheckoutEntry(LocalDate.now(), LocalDate.now().plusDays(7), copyOfBook);

        if(member.getCheckoutRecord().addEntry(checkoutEntry))
        {
            copyOfBook.setBorrowed(true);
            return true;
        }

        return false;
    }
}
