package controller;

import dataAccess.DataAccess;
import dataAccess.DataAccessImpl;
import library.Book;
import library.CheckoutEntry;
import library.Copy;
import library.Author;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by 985366 on 9/13/2016.
 */
public class AutherController {
    private static AutherController instance = new AutherController();
    private DataAccess<String, Author> authors = new DataAccessImpl<String,  Author>();

    private AutherController(){};

    public static AutherController getInstance()
    {
        return instance;
    }

    public boolean addAuthor(Author author)
    {
        return authors.add(author.getPerson().getId(), author);
    }

    public Author getAuthor(String id)
    {
        return authors.get(id);
    }

    public List<Author> getAll()
    {
        return authors.getAll();
    }

    public boolean updateAuthor(Author author) {
        return authors.update(author.getPerson().getId(), author);
    }

    public DataAccess<String, Author> getAuthors()
    {
        return authors;
    }
}
