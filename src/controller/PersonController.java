package controller;

import dataAccess.DataAccess;
import dataAccess.DataAccessImpl;
import library.Book;
import library.CheckoutEntry;
import library.Copy;
import library.Person;

import java.time.LocalDate;

/**
 * Created by 985366 on 9/13/2016.
 */
public class PersonController {
    private static PersonController instance = new PersonController();
    private DataAccess<String, Person> Persons = new DataAccessImpl<String,  Person>();
    private DataAccess<String, Book> books = new DataAccessImpl<String, Book>();

    private PersonController(){};

    public static PersonController getInstance()
    {
        return instance;
    }

    public boolean addPerson(Person Person)
    {
        return Persons.add(Person.getId(), Person);
    }

    public Person getPerson(String id)
    {
        return Persons.get(id);
    }

    public DataAccess<String, Person> getPersons()
    {
        return Persons;
    }
}
